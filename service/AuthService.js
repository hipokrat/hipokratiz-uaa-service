require("dotenv").config();
const config = require("config");
const {
  authserver: { url },
} = config;
const { FusionAuthClient } = require("@fusionauth/typescript-client");
const got = require("got");

var client = new FusionAuthClient(process.env.fusionauth_apikey, url);
exports.CreateUser = function (body, callback) {
  body.sendSetPasswordEmail = true;

  client
    .createUser("", body)
    .then(function (clientResponse) {
      // console.log(JSON.stringify(clientResponse.successResponse, null, 8))
      callback(null, clientResponse);
    })
    .catch(function (error) {
      console.log("ERROR: ", JSON.stringify(error));
      callback(error);
    });
};

exports.userRegister = function (body, callback) {

    var register = {
        "registration": {
            "applicationId": body.applicationId
        }
    }

    console.log(register)

    client.register(body.userId, register)
        .then(function (clientResponse) {
            // console.log(JSON.stringify(clientResponse.successResponse, null, 8)) 
            callback(null, clientResponse);
        })
        .catch(function (error) {
            console.log("ERROR: ", JSON.stringify(error, null, 8))
            callback(error);
        });
}

exports.Login = function (body, callback) {
  var loginObj = {
    loginId: body.username,
    password: body.password,
    applicationId: body.applicationId,
  };
  console.log("API Key -> " + process.env.fusionauth_apikey);
  console.log("URL -> " + url);
  client
    .login(loginObj)
    .then(function (clientResponse) {
      // console.log(JSON.stringify(clientResponse, null, 8))
      if (clientResponse.statusCode == 202) {
        callback("fusionauth status unauthorised");
      }
      callback(null, clientResponse);
    })
    .catch(function (error) {
      console.log("ERROR: ", JSON.stringify(error, null, 8));
      callback(error);
    });
};

exports.Validate = function (token, callback) {
  client
    .validateJWT(token)
    .then(function (clientResponse) {
      // console.log(JSON.stringify(clientResponse, null, 8))
      callback(null, clientResponse);
    })
    .catch(function (error) {
      console.log("ERROR: ", JSON.stringify(error, null, 8));
      callback(error);
    });
};

exports.SetPassword = function (body, callback) {
  console.log(body);

  var changePasswordRequest = {
    password: body.password,
  };
  client
    .changePassword(body.changePasswordId, changePasswordRequest)
    .then(function (clientResponse) {
      console.log(JSON.stringify(clientResponse, null, 8));
      callback(null, clientResponse);
    })
    .catch(function (error) {
      console.log("ERROR: ", JSON.stringify(error, null, 8));
      callback(error);
    });
};

exports.ForgotPassword = function (body, callback) {
  console.log(body);

  var forgotPasswordRequest = {
    loginId: body.email,
    applicationId: body.applicationId,
    email: body.email,
    sendForgotPasswordEmail: true,
  };
  console.log(forgotPasswordRequest);
  client
    .forgotPassword(forgotPasswordRequest)
    .then(function (clientResponse) {
      console.log(JSON.stringify(clientResponse, null, 8));
      callback(null, clientResponse);
    })
    .catch(function (error) {
      console.log("ERROR: ", JSON.stringify(error, null, 8));
      callback(error);
    });
};

exports.ChangePassword = function (body, callback) {
  var changePasswordRequest = {
    currentPassword: body.currentPassword,
    loginId: body.email,
    password: body.password,
  };
  console.log(changePasswordRequest);
  client
    .changePasswordByIdentity(changePasswordRequest)
    .then(function (clientResponse) {
      console.log(JSON.stringify(clientResponse, null, 8));
      callback(null, clientResponse);
    })
    .catch(function (error) {
      console.log("ERROR: ", JSON.stringify(error, null, 8));
      callback(error);
    });
};

exports.ResendOnboardingEmail = function (body, callback) {
  let changePasswordId;
  let email = body.email;

  let resendPasswordSettingEmail = {
    loginId: email,
    applicationId: body.applicationId,
    email: email,
    sendForgotPasswordEmail: false,
  };

  client
    .forgotPassword(resendPasswordSettingEmail)
    .then(async (clientResponse) => {
      console.log(JSON.stringify(clientResponse, null, 8));
      return (changePasswordId = clientResponse.response.changePasswordId);
    })
    .then(async () => {
      const userDetails = await client.retrieveUserByEmail(email);
      return userDetails
        ? userDetails.response.user.firstName.concat(
            " ",
            userDetails.response.user.lastName
          )
        : "Unknown User";
    })
    .then(async (userName) => {
      let mailResponse = await sendEmail(email, userName, changePasswordId);
      callback(null, mailResponse.body);
    })
    .catch(function (error) {
      console.log("ERROR: ", JSON.stringify(error, null, 8));
      callback(error);
    });
};

let sendEmail = async (email, username, changePasswordId) => {
  let mailContent =
    "Hi " +
    username +
    "," +
    "<p>This is Vinodh, founder & CEO of Hipokratiz.</p>" +
    "<p>Together with my team, I'm thrilled & excited to welcome you on board. </p>" +
    "<p>At Hipokratiz, we're working towards eliminating the various communication, cost, and commute barriers to healthcare - one step at a time.</p>" +
    "<p>To achieve the same, we'll be providing you with various tools, systems, and software so you can eliminate the barriers, help more patients and grow your dental practice.</p>" +
    "<p>As a 24*7 dental virtual receptionist, Irine is the first tool you need to help you convert your website visitors into patient leads and appointments. </p>" +
    "<p>And, to help you set up Irine on your website, We have created your Irine account inside the Hipokratiz platform. </p>" +
    "<p>As a next step, please go ahead, create your password and access your Irine account in the link below. </p>" +
    "<p>" +
    "<a href='" +
    config.get("client.practitioner_app_url") +
    "/password-setting/" +
    changePasswordId +
    "'>" +
    "Setup your password" +
    "</a>" +
    "</p>" +
    "<p>Use the below link if the Setup your password link is not working.</p><p><a href='" +
    config.get("client.practitioner_app_url") +
    "/resendOnboardingEmail'>" +
    "Resend instructions to setup the password" +
    "</a></p>" +
    "<p>If you need anything, just reply to this message. </p>" +
    "<p>We're here to help you.</p>" +
    "<br>" +
    "<p style='margin-bottom: 0px;'>Vinodh Subramanian</p>" +
    "<p style='margin-bottom: 0px;margin-top: 0px;'>Founder & CEO</p>" +
    "<p style='margin-top: 0px;'>" +
    "<a href='https://hipokratiz.com'>Hipokratiz.com</a>" +
    "</p>";
    console.log(mailContent);
  return await got.post("http://localhost:8015/conversation/email/", {
    json: {
      senderEmail: "support@hipokratiz.com",
      receiverEmail: email,
      subject: "Setup your password",
      mesg: mailContent,
    },
  });
};
