var express = require('express');
var cors = require('cors')
const jwt_decode = require('jwt-decode');
var logger = require('./utils/logger')

var bodyParser = require('body-parser');var app = express();
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());var routes = require('./routes');
// app.use(logReqRes)
app.use(setUserFromJWT)
app.use('/', routes);module.exports = app;


function setUserFromJWT(req, res, next) {
    if (req.headers['authorization']) {
        req.user = jwt_decode(req.headers['authorization'])
    }
    next()
}

function logReqRes(req, res, next) {
    logger.info({ reqPath: req.path, reqHeader: req.headers, reqBody: req.body })
    var oldWrite = res.write,
        oldEnd = res.end;
    var chunks = [];
    res.write = function (chunk) {
        chunks.push(chunk);
        return oldWrite.apply(res, arguments);
    };
    res.end = function (chunk) {
        if (chunk)
            chunks.push(chunk);
        var body = Buffer.concat(chunks).toString('utf8');
        logger.info({resCorId: req.headers['x-internal-correlationid'], resBody: JSON.parse(body) })
        oldEnd.apply(res, arguments);
    };
    next()
}
