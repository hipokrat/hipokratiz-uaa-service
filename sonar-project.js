const sonarqubeScanner = require('sonarqube-scanner')
sonarqubeScanner({
  serverUrl: 'https://sonar.dev.hipokratiz.com/',
  options: {
    'sonar.sources': '.',
    'sonar.inclusions': '*/**', // Entry point of your code
    'sonar.login': 'ca00b8aeea5ecc4b6933e87b26fa634732cd815c',
    'sonar.language':'javascript',
    'sonar.sourceEncoding': 'UTF-8',
    'sonar.exclusions': '*/*.test.js',
    'sonar.javascript.lcov.reportPaths': 'coverage/lcov.info',
    'sonar.testExecutionReportPaths':'coverage/test-reporter.xml',
    'sonar.coverage.exclusions': '**/*.test.js,**/*.mock.ts,node_modules/*,coverage/lcov-report/*',
    'sonar.javascript.file.suffixes': '.js',
    'sonar.eslint.reportPaths':'report.json'
  }
}, () => {})
