require('dotenv').config();
const { transports, createLogger, format } = require('winston');
const config = require('config');
const { log: { level }, service: { name } } = config;


const logger = createLogger({
    level: level.toLowerCase(),
    defaultMeta: { service: name.toUpperCase(), environment: process.env.NODE_ENV.toUpperCase() },
    format: format.combine(
        format.timestamp(),
        format.json()
    ),
    transports: [
        new transports.Console()
    ]
});

// if (process.env.NODE_ENV == 'PROD') {
logger.add(
    new transports.File({ filename: '../logs/' + name.toLowerCase() + '.log' })
);
//   }

module.exports = logger;