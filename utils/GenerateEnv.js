const fs = require("fs")
const aws = require('./AwsSecret');

(async () => {
    
    const secretsString = await aws.getSecret()
    fs.writeFileSync(".env", secretsString);
    fs.appendFileSync('.env', 'NODE_ENV=' + process.argv[2]);
    
})();