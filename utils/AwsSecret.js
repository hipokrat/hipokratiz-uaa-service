const config = require('config');

// Load the AWS SDK
var AWS = require('aws-sdk'),
    region = config.get('aws.region'),
    secretName = config.get('aws.secretName'),
    secret,
    decodedBinarySecret;

// Create a Secrets Manager client
var client = new AWS.SecretsManager({
    region: region
});

async function getSecret() {
    return new Promise((resolve, reject) => {
        client.getSecretValue({ SecretId: secretName }, function (err, data) {
            if (err) {
                reject(err);
            }
            else {
                if ('SecretString' in data) {
                    secret = data.SecretString;
                } else {
                    let buff = new Buffer.from(data.SecretBinary, 'base64');
                    decodedBinarySecret = buff.toString('ascii');
                }
            }
            let secretsString = "";
            let secretsJSON = JSON.parse(secret)
            Object.keys(secretsJSON).forEach((key) => {
                secretsString += `${key}=${secretsJSON[key]}\n`;
            });
            resolve(secretsString)
        });
    });
}

module.exports.getSecret = getSecret