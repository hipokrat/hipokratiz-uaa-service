module.exports = {
    parser: "@babel/eslint-parser",
    parserOptions: { 
      requireConfigFile : false
    },
    plugins: ["sonarjs"],
      extends: ["plugin:sonarjs/recommended"]  
};
