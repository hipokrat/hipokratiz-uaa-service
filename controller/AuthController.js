var authService = require("../service/AuthService");

exports.createUser = async function (req, res) {
  authService.CreateUser(req.body, async function (err, result) {
    if (err) return res.json({ status: "error", data: { error: err } });
    res.json({ status: "success", data: { user: result } });
  });
};

exports.userRegister = function (req, res) {
  authService.userRegister(req.body, function (err, result) {
    if (err) {
      return res.json({ status: "error", data: { error: err } });
    }
    res.json({ status: "success", data: { result: result } });
  });
};

exports.login = function (req, res) {
  authService.Login(req.body, function (err, result) {
    if (err)
      return res.status(401).json({ status: "error", data: { error: err } });
    res.json({ status: "success", data: { accessToken: result } });
  });
};

exports.validate_token = function (req, res) {
  authService.Validate(req.body.token, function (err, result) {
    if (err) return res.send(err.message);
    res.send(result);
  });
};

exports.SetPassword = function (req, res) {
  authService.SetPassword(req.body, function (err, result) {
    if (err) return res.status(400).json(err.message);
    res.send(result);
  });
};

exports.ForgotPassword = function (req, res) {
  authService.ForgotPassword(req.body, function (err, result) {
    if (err) return res.status(400).send(err.message);
    res.send(result);
  });
};

exports.ChangePassword = function (req, res) {
  authService.ChangePassword(req.body, function (err, result) {
    if (err) return res.status(400).send(err.message);
    res.send(result);
  });
};

exports.ResendOnboardingEmail = function (req, res) {
  authService.ResendOnboardingEmail(req.body, function (err, result) {
    if (err) return res.status(400).send(err.message);
    res.send(result);
  });
};
