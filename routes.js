var express = require('express');
var router = express.Router();
var authController = require('./controller/AuthController');

router.post('/admin/user', authController.createUser);
router.post('/admin/user/register', authController.userRegister);
router.post('/login', authController.login);
router.post('/validateToken', authController.validate_token);
router.post('/admin/user/setpassword', authController.SetPassword);
router.post('/admin/user/forgotpassword', authController.ForgotPassword);
router.post('/admin/user/changepassword', authController.ChangePassword);
router.post('/resendOnboardingEmail', authController.ResendOnboardingEmail);

module.exports = router;